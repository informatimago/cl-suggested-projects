* Library for two-dimensional graph layout
  :PROPERTIES:
  :CUSTOM_ID: library-for-two-dimensional-graph-layout
  :END:
** Description
   :PROPERTIES:
   :CUSTOM_ID: description
   :END:
The purpose of this project is to create a library for creating a good
two-dimensional layout of graphs, in the spirit of
[[http://www.graphviz.org][Graphviz]]. In fact, the algorithms used by
Graphviz are documented in
[[http://www.graphviz.org/documentation/TSE93.pdf][this document]], so a
good start would be to implement those algorithms in Common Lisp.

[[https://gitlab.com/projects/new][Create a new gitlab project]]
--------------

robert.strandh@gmail.com
